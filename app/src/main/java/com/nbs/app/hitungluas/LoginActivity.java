package com.nbs.app.hitungluas;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements ClientCallback {
    private EditText edtNim;
    private EditText edtPwd;
    private Button btnLogin;
    private RequestQueue requestQueue;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        edtNim = (EditText)findViewById(R.id.edt_nim);
        edtPwd = (EditText)findViewById(R.id.edt_pwd);
        btnLogin = (Button)findViewById(R.id.btn_login);


        final NetworkService networkService = new NetworkService(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait");


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                networkService.login(edtNim.getText().toString(), edtPwd.getText().toString(), LoginActivity.this);
                progressDialog.show();
            }
        });
    }

    @Override
    public void onSucceeded() {
        Toast.makeText(LoginActivity.this, "Berhasil Login", Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();
    }

    @Override
    public void onFailed() {
        Toast.makeText(LoginActivity.this, "Gagal Login", Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();
    }
}
