package com.nbs.app.hitungluas;

/**
 * Created by pandhu on 01/07/16.
 */
public interface ClientCallback {
    void onSucceeded();
    void onFailed();

}
